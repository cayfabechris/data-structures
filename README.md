Here you will find my projects related to data structures!

More projects are being continually added (Late 2018), so come back for new projects and updates.

These projects are meant to implement the main topics of data structures (arrays, stacks, queues, etc.)

To view a project, select a folder. In the folder, you will see a .java or .py file to see my code. At the bottom of the folder page, you will see a gif of the program running or if it is an mp4, you can see it that way.

**VERY IMPORTANT NOTE TO STUDENTS**

If you are a student at any school or university, do not copy and paste or plagarize any of this work to submit as your own. Changing variable names or syntax will still constitute as cheating.
You can get ideas from these projects on how you might implement them, but ultimately you must do the work yourself.
This not only benefits you, but everyone else, as you learn how to become a better programmer and thinker.