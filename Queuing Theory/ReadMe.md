This is Queuing Theory!

Main Class: QueuingTheory.java

Additional Required Classes: Queue.java, QNode.java, CustomTimerTask.java, BarberChair.java

---Data Structure: Queue---

Goal: Simulate a barber shop using queues.

Input: (No user input)

Implementation: A queue will be created for each area that a patron could possibly wait/be serviced: a cash register, for when patrons have received their haircut, 
a barber chair, for patrons that are receiving their haircut, a couch, when all barbers are occupied, and a line, when the couch is full of patrons.
New patrons will enter to the best available queue and once they have paid at the cash register, they will leave.

![Queuing Theory Gif](Queuing Theory/QueuingTheory.gif)
