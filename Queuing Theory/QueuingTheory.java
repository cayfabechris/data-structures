/* Program name: QueuingTheory (Barber shop)
 * 
 * 
 * Description: Simulate a barber shop with patrons coming in and receiving haircuts. If all barbers are occupied,
 * new patrons will then be moved to the couch. If the couch limit has been reached, patrons must stand in line.
 * Once a barber is available, if there are any patrons sitting on the couch, the first person on the couch will be next.
 * Once a spot on the couch is free, the first person in line will move to the couch. Use queues to achieve this.
 * 
 * 
 * Additional required classes: Queue.java, QNode.java, BarberChair.java, CustomTimerTask.java
 * 
 * 
 * IMPORTANT: If you want to make patrons come in faster/slower, read the notes found in createQueue()
 * and adjust the Thread.sleep() speeds. Note this will affect whether patrons have to stand in line,
 * as the faster they come in, the more likely they will have to sit on the couch or stand. 
 * To change the barber or cashier speeds, increase the maxTime variable for longer processing speeds,
 * and decrease for faster processing speeds. This variable can be found in the randomBarberSpeed() or
 * randomCashierSpeed(), simply change the maxTime in the one you want to increase/decrease.
 * Changing the patron limit found as a global variable (patronLimit) will also affect the length of the program.
 * Be careful running this program multiple times. Timer.schedule() operations from a former run of the program may show up on the console.
 * It is unknown if this is a result of using the Eclipse IDE, but to fix this, simply restart your IDE.
 * 
 * 
 * Programmer: Christian Rodriguez
*/

import java.util.Timer;

public class QueuingTheory{
	
	//Current patron count
	static int patronCount = 0;
	
	//Patron limit
	static int patronLimit = 40;
	
	//Timer that will be used to schedule enqueuing and dequeuing 
	static Timer timer = new Timer();
	
	//Checks whether all patrons within our patron limit have been serviced
	static boolean allPatronsServiced = false;
	
	//Limit of people that can be on the couch
	static int couchLimit = 0;
	
	//Determines whether a barber is occupied
	static boolean barberOccupied;
	
	//Main method
	public static void main(String [] args) throws InterruptedException {
		greetings();
	    createQueue();
		}
	
	//Greeting
	public static void greetings(){
		System.out.println("\t////Howdy! Welcome to Christian's Hairscaping! AKA Barbershop\\\\\\\\\n");
	}
	
	public static void createQueue() throws InterruptedException{
		//The first area where clients wait if the couch is filled
		Queue line = new Queue("line");
		
		//The area where clients will wait if all barbers are busy
		Queue couch = new Queue("couch");
		
		//This is where clients pay after they have received their haircut
		Queue cashier = new Queue("cashier");
		
		//An array of barbers of the type BarberChair, the method is for cleaner code and to generate our barbers.
		BarberChair [] barbers = generateBarbers();
		
		//Patron data is stored during the haircut to then transfer to the cashier after the haircut is complete.
		QNode patronData, patronDataX, patron;
				
		//Patron chair that will store the current patron
		BarberChair patronChair, patronChairX;
		
		//Cashier finish times, all values will be extracted from randomizeCashierSpeed().
		long cFinishTime, cFinishTimeX;
		
		//Barber finish times, all values will be extracted from randomizeBarberSpeed().
		long bFinishTime, bFinishTimeX;
		
		
		while(allPatronsServiced == false){
			
			while(patronCount < patronLimit){
				
				//New patron has entered and has been created.
				patronCount++;
				patron = new QNode(patronCount);
				
				/*This thread is a buffer that prevents patrons from flooding into the store. 16000 (16 seconds) is the default value, 
				  and Math.Random allows the value to be from 1 to 16 seconds. Decrease the default value to make the program run faster,
				  increase it to make it run slower. Note: faster times are more likely to cause patrons to sit on the couch and stand on line.
				 */
				
				Thread.sleep((long) (Math.random() * 16000));
		        System.out.println("\t!---(Patron " + patron.key + " has entered)---!\n");
			
				//Is there a barber available?
				if(barberAvailable(barbers)){
				//Yes
					
					//Is anyone on the couch?
					//Yes
					if(couch.front != null){
					
					patronChairX = barbers[freeBarber(barbers)];
					patronChairX.patron = couch.front;
					patronDataX = patronChairX.patron;
					bFinishTimeX = randomBarberSpeed(patronChairX);
					cFinishTimeX = randomCashierSpeed();
					System.out.println("Patron " + patronChairX.patron.key + " has left the couch and is with barber " + patronChairX.barber + ".\n");
					timer.schedule(new CustomTimerTask(2, couch, patronChairX.patron.key), 0);
					//Is someone on line?
					//Yes
					if(line.front != null){
						timer.schedule(new CustomTimerTask(1, couch, line.front.key), 0);
						timer.schedule(new CustomTimerTask(2, line, line.front.key), 0);
						timer.schedule(new CustomTimerTask(1, line, patron.key), 0);
					}
					//No
					else{
						timer.schedule(new CustomTimerTask(1, couch, patron.key), 0);
					}
					timer.schedule(new CustomTimerTask(3, patronChairX), bFinishTimeX);
					timer.schedule(new CustomTimerTask(1, cashier, patronDataX.key), bFinishTimeX + 1000);
					timer.schedule(new CustomTimerTask(2, cashier, patronChairX.patron.key),bFinishTimeX + 1000 + cFinishTimeX);
					}
					
					//No one on the couch
					//No
					else{
						patronChair = barbers[freeBarber(barbers)];
						patronChair.patron = patron;
						patronData = patronChair.patron;
						bFinishTime = randomBarberSpeed(patronChair);
						cFinishTime = randomCashierSpeed();
						System.out.println("Patron " + patronData.key + " is with barber " + patronChair.barber + ".\n");
						timer.schedule(new CustomTimerTask(3, patronChair), bFinishTime);
						timer.schedule(new CustomTimerTask(1, cashier, patronData.key), bFinishTime + 1000);
						timer.schedule(new CustomTimerTask(2, cashier, patronData.key), bFinishTime + 1000 +  cFinishTime);

					}
					
				}
				
				
				//All barbers occupied
				else if(couchLimit < 4) {
					//Is there a line?
					//Yes
					if(line.front != null){
					couchLimit++;
					System.out.println("Couch spot freed. Patron " + line.front.key + " has moved from the line to the couch.\n");
					timer.schedule(new CustomTimerTask(1, couch, line.front.key), 0);
					timer.schedule(new CustomTimerTask(2, line, line.front.key), 0);
					timer.schedule(new CustomTimerTask(1, line, patron.key), 0);
					}
					
					//Current patron sits on the couch
					else{
					couchLimit++;
					System.out.println("All barbers occupied. Patron " + patron.key + " sits on the couch. They are person "+ couchLimit + " on the couch.\n");
					timer.schedule(new CustomTimerTask(1, couch, patron.key), 0);
					}
				}
				
				//Patron joins line, couch is full
				else{
					System.out.println("Patron " + patron.key + " is standing in line.\n");
					timer.schedule(new CustomTimerTask(1, line, patron.key), 0);
				}
			}
			
			System.out.println("Thank you for coming! We are closing as we've reached our patron limit! All remaining customers will be serviced." + "\n");

			//Patron limit has been reached, but there are still remaining patrons on the couch and/or line
			while(couch.front != null || line.front != null || barberOccupied(barbers) || cashier.front != null) {
				
				//Thread buffer that stops operations running too fast. Similar to the thread sleep from the last while loop.
				//The default value is 10000 (10 seconds), and the range is from 1 to 10 seconds.
				//To make it faster, lower the default value. To make it slower, increase it.
				Thread.sleep((long) (Math.random() * 10000));

				//Is there an available barber?
				if(barberAvailable(barbers)){
					
					//Is there anyone on the couch?
					
					//Yes
					if(couch.front != null) {
					patronChairX = barbers[freeBarber(barbers)];
					patronChairX.patron = couch.front;
					patronDataX = patronChairX.patron;
					bFinishTimeX = randomBarberSpeed(patronChairX);
					cFinishTimeX = randomCashierSpeed();
					System.out.println("Patron " + patronChairX.patron.key + " has left the couch and is with barber " + patronChairX.barber + ".\n");
					timer.schedule(new CustomTimerTask(2, couch, patronChairX.patron.key), 0);
					
					//Is there a line?
					if(line.front != null){
						timer.schedule(new CustomTimerTask(1, couch, line.front.key), 0);
						timer.schedule(new CustomTimerTask(2, line, line.front.key), 0);
					}
					
					timer.schedule(new CustomTimerTask(3, patronChairX), bFinishTimeX);
					timer.schedule(new CustomTimerTask(1, cashier, patronDataX.key), bFinishTimeX + 1000);
					timer.schedule(new CustomTimerTask(2, cashier, patronChairX.patron.key),bFinishTimeX + 1000 + cFinishTimeX);
					couchLimit--;
					}
					
					//No
					else {
						continue;
					}
				}
				
				
				//All barbers occupied, patron sits on couch
				else if(couchLimit < 4) {
					if(line.front != null){
					couchLimit++;
					System.out.println("Couch spot freed. Patron " + line.front.key + " has moved from the line to the couch.\n");
					timer.schedule(new CustomTimerTask(1, couch, line.front.key), 0);
					timer.schedule(new CustomTimerTask(2, line, line.front.key), 0);
					}
					
					else{
					continue;
					}
				}
				
				//Patron joins line, couch is full
				else{
					continue;
				}
			}
			
			//Timer is cancelled for efficiency, all scheduling has been completed.
			timer.schedule(new CustomTimerTask(4, timer), 1000);
			allPatronsServiced = true;
		}
			
	}
	
	//This will generate the array of barbers being used
	public static BarberChair [] generateBarbers() {
		
		BarberChair barber1 = new BarberChair(1);
		BarberChair barber2 = new BarberChair(2);
		BarberChair barber3 = new BarberChair(3);

		BarberChair [] barbers = {barber1, barber2, barber3};
		
		return barbers;
	}
	
	//Determines whether a barber is currently available
	public static boolean barberAvailable(BarberChair [] barbers){
		for(int i = 0; i < barbers.length; i++){
			if(barbers[i].patron == null){
				return true;
			}
		}
		
		return false;
	}
	
	//Determines whether any barber is currently occupied
	public static boolean barberOccupied(BarberChair [] barbers) {
		for(int i = 0; i < barbers.length; i++) {
			if(barbers[i].patron != null) {
				barberOccupied = true;
				return barberOccupied;
			}
			else {
				continue;
			}
		}
		barberOccupied = false;
		return barberOccupied;
	}
	
	//If there is a free barber, this will return which barber is available
	public static int freeBarber(BarberChair [] barbers){
		if(barberAvailable(barbers)){
			for(int i = 0; i < barbers.length; i++){
				if(barbers[i].patron == null){
					return i;
				}
			}
			
		}
		return -1;
	}
	
	//Depending on the barber, different values will be generated to determine how fast a barber performs a haircut.
	public static long randomBarberSpeed(BarberChair barber) {
		barber.speed = Math.random();
		long maxTime = 30000;
		
		//Barber 1
		if(barber.barber == 1) {
			
			//Fast, barber 1 has a 70% chance of having a fast haircut
			if(barber.speed >= 0.30) {	
			//System.out.println("Barber 1 will cut your hair in no time! \n");
			
			//Minimum time: 4 seconds (0.99) Max time: 5 seconds (0.95)
			if(barber.speed >= 0.95){
			
			maxTime = (long) (maxTime - ((barber.speed * 10000)/ 0.495));
			return maxTime;
			}
			
			//Minimum time: 5 seconds (0.94) Max time: 6 seconds (0.90)
			else if(barber.speed < 0.95 && barber.speed >= 0.90){
				maxTime = (long) (maxTime - ((barber.speed * 10000)/ 0.494));
				return maxTime;
			}
			
			//Minimum time: 6 seconds (0.89) Max time: 7 seconds (0.85)
			else if(barber.speed < 0.90 && barber.speed >= 0.85){
				maxTime = (long) (maxTime - ((barber.speed * 10000)/ 0.494));
				return maxTime;
			}
			
			//Everything down will result similar to 10 seconds
			else if(barber.speed < 0.85 && barber.speed >= 0.80){
				maxTime = (long) (maxTime - ((barber.speed * 10000)/ 0.5));
				return maxTime;
			}
			
			else if(barber.speed < 0.80 && barber.speed >= 0.50){
				maxTime = (long) (maxTime - ((barber.speed * 10000)/ 0.5));
				return maxTime;
			}
			
			else{
				maxTime = (long) (maxTime - ((barber.speed * 10000)/ 0.3));
				return maxTime;
			}
			
			}
			
			//Medium, barber 1 has a 20% chance of performing a medium speed haircut
			else if(barber.speed < 0.30 && barber.speed >= 0.10) {
				//System.out.println("Barber 1 will give you an average length haircut. \n");
				maxTime = (long) (maxTime - ((barber.speed * 10000)/ 0.263));
				return maxTime;
				
			}
			
			//Slow, barber 1 has a 10% chance of performing a slow haircut
			else{
				//System.out.println("Barber 1 isn't feeling their best today...\n");
				return maxTime;
			}
		}
		
		//Barber 2
		else if(barber.barber == 2) {
			
			//Fast, barber 2 has a 50% chance of giving a fast haircut
			if(barber.speed >= 0.50) {
				//System.out.println("Barber 2 will cut your hair in no time! \n");

				if(barber.speed >= 0.95){
					
					maxTime = (long) (maxTime - ((barber.speed * 10000)/ 0.495));
					return maxTime;
					}
					
				//Minimum time: 5 seconds (0.94) Max time: 6 seconds (0.90)
				else if(barber.speed < 0.95 && barber.speed >= 0.90){
						maxTime = (long) (maxTime - ((barber.speed * 10000)/ 0.494));
						return maxTime;
					}
				
				else if(barber.speed < 0.90 && barber.speed >= 0.85){
					maxTime = (long) (maxTime - ((barber.speed * 10000)/ 0.772));
					return maxTime;
				}
				
				else if(barber.speed < 0.85 && barber.speed >= 0.80){
					maxTime = (long) (maxTime - ((barber.speed * 10000)/ 0.666));
					return maxTime;
				}
				
				else if(barber.speed < 0.80 && barber.speed >= 0.50){
					maxTime = (long) (maxTime - ((barber.speed * 10000)/ 0.5));
					return maxTime;
				}
				
			}
			
			//Medium, barber 1 has a 20% chance of performing a medium speed haircut
			else if(barber.speed < 0.50 && barber.speed >= 0.30) {
				//System.out.println("Barber 2 will give you an average length haircut. \n");
				maxTime = (long) (maxTime - ((barber.speed * 10000)/ 0.333));
				return maxTime;
			}
			
			//Slow, barber 2 has a 20% chance of performing a slow haircut
			else{
				//System.out.println("Barber 2 isn't feeling their best today...\n");
				return maxTime;
			}
		}
		
		//Barber 3
		else {
			//Fast, barber 3 has a 30% chance of giving a fast haircut
			if(barber.speed >= 0.70) {
				//System.out.println("Barber 3 may have a lucky charm today... \n");

				if(barber.speed >= 0.97){
					
					maxTime = (long) (maxTime - ((barber.speed * 10000)/ 0.495));
					return maxTime;
					}
				else if(barber.speed < 0.97 && barber.speed >= 0.90){
					maxTime = (long) (maxTime - ((barber.speed * 10000)/ 0.5));
					return maxTime;
				}
				
				else if(barber.speed < 0.90 && barber.speed >= 0.85){
					maxTime = (long) (maxTime - ((barber.speed * 10000)/ 0.4));
					return maxTime;
				}
				
				else if(barber.speed < 0.85 && barber.speed >= 0.80){
					maxTime = (long) (maxTime - ((barber.speed * 10000)/ 0.7));
					return maxTime;
				}
				
				else if(barber.speed < 0.80 && barber.speed >= 0.50){
					maxTime = (long) (maxTime - ((barber.speed * 10000)/ 0.8));
					return maxTime;
				}
			}
			
			//Medium, barber 3 has a 20% chance of performing a medium speed haircut
			else if(barber.speed <= 0.70 && barber.speed >= 0.50) {
				//System.out.println("Barber 3 is doing okay. \n");
				maxTime = (long) (maxTime - ((barber.speed * 10000)));
				return maxTime;
				
			}
			
			//Slow, barber 3 has a 50% chance of performing a slow haircut
			else{
				//System.out.println("Barber 3 is trying their best okay...\n");
				return maxTime;
			}
			
		}
		
		return maxTime;
	}
	
	//Random cashier processing speed
	public static long randomCashierSpeed(){
		double speed = Math.random();
		long maxTime = 6000;
		
		if(speed >= 0.3){
			return 3000; 
		}
		
		else{
			maxTime = (long) (maxTime - ((speed * 10000))/2.9);
			return maxTime;
		}
	}
}

//Thanks for reading my code ~ CR :)