import java.util.TimerTask;
import java.util.Timer;

public class CustomTimerTask extends TimerTask{
	
	int opCode;
	QNode front, rear, ptr;
	int key;
	Queue q;
	BarberChair chair;
	int patron;
	Timer t;

	//Enqueue, works with opCode 1
	 public CustomTimerTask(int opCode, Queue q, int key) {
		 this.opCode = opCode;
		 this.q = q;
		 this.key = key;
     }
	 
	 //Dequeue, works with opCode 2
	 public CustomTimerTask(int opCode, Queue q) {
		 this.opCode = opCode;
		 this.q = q;
     }
	 
	 //Barber patron removal, works with opCode 3
	 public CustomTimerTask(int opCode, BarberChair chair) {
		 this.opCode = opCode;
		 this.chair = chair;
     }
	 
	//Timer cancel, works with opCode 4 
	public CustomTimerTask(int opCode, Timer t) {
		 this.opCode = opCode;
		 this.t = t;
	 }
	
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		 
		//Enqueue
        if(opCode == 1){
        // Create a new LL node 
        QNode temp = new QNode(key); 

       
        // If queue is empty, then new node is front and rear both 
        if (q.rear == null) 
        { 
           System.out.println("\t!---(Patron " + this.key + " has been enqueued to the " + q.name + ")---!\n");
           q.front = q.rear = temp; 
           return; 
        } 

       System.out.println("\t!---(Patron " + this.key + " has been enqueued to the " + q.name + ")---!\n");
        // Add the new node at the end of queue and change rear 
        q.rear.next = temp; 
        q.rear = temp;
        }
        
        //Dequeue
        else if(opCode == 2){
       	// If queue is empty, return NULL. 
  	       System.out.println("\t!---(Patron " + this.key + " has left the " + q.name + ")---!\n");
        	
            if (q.front == null){ 
     	       System.out.println(q.name.substring(0,1).toUpperCase() + q.name.substring(1, q.name.length()) + " is empty\n");
               //return null; 
            }
            
            else if(q.front != null) {
           
            // Store previous front and move front one node ahead 
            QNode temp = q.front;
            q.front = q.front.next;
           
            // If front becomes NULL, then change rear also as NULL 

            //System.out.println("Patron " + temp.key + " has been dequeued from " + q.name + "\n");

            if (q.front == null) 
               q.rear = null; 
            //return temp; 
        }
            //Barber dequeue
      
        }
        
        else if (opCode == 3){
        	System.out.println("Patron " + chair.patron.key + " has received a haircut from barber " + chair.barber + ".\n");
        	chair.patron = null;
        }
        
        else {
        	System.out.println("All patrons have been serviced! Thank you for visiting Christian's Hairscaping! AKA Barbershop :)");
        	t.cancel();
        }
	}

}
