/*
 * Program: Tower of Hanoi
 * 
 * Purpose: Make Tower of Hanoi using stacks and display them. Allow a user to select the number of disks.
 * The valid user input is from 3 to 10.
 * 
 * Programmer: Christian Rodriguez
 * 
 * Additional required classes: Stack.java, StackNode.java
 * 
 * IMPORTANT: This program uses a Thread.sleep() method to prevent all the steps from coming out at once.
 * Each step will display after 3 seconds (3000). To make it faster lower the value, to make it slower, increase it.
 * To make all the steps come out at once, comment out the Thread.sleep() found in the solution() method.
 * 
 * 
 */

import java.util.Scanner;

public class TowerOfHanoi {
	
	//Keeps track of the current step in the puzzle
	static int stepCounter = 0;
	
	//The main array of stacks/poles used to solve the puzzle
	static Stack [] poles = new Stack[3];
	
	//The grid size/number of disks input by the user
	static int gridSize = 0;
	
	//This array will order the stacks/poles object correctly, which will then be used to display the poles
	static Stack [] displayPoles = new Stack[3];
	
	//Used to return the total number of steps a tower will take
	static int steps = 0;
	
	
	public static void main(String [] args) throws InterruptedException {
		
		
		System.out.println("Welcome to Tower Of Christian! Please enter a number between 3 and 10 (Inclusive).");
		
		//User input
		int diskNum = userInput();
		
		//Input stored as gridSize
		gridSize = diskNum;	
		
		//The initial print separate from the actual solving
		initialPrint();
		
		poles = createPoles(diskNum);
		solution(diskNum, poles[0], poles[1], poles[2]);
		
	}
	
	public static Stack [] getPoles(){
		return displayPoles;
	}

	//Create stacks/poles that will be used
	public static Stack [] createPoles(int diskNum){
		
		//System.out.println("Create Poles Reached");

		Stack pole = new Stack();
		pole.name = "A";
		
		//This will populate the disk number
		for(int i = diskNum; i > 0; i--){
			pole.Push(i - 1);
		}
		
		Stack pole1 = new Stack();  
		pole1.name = "B";
		
		Stack pole2 = new Stack();
		pole2.name = "C";
		
		Stack [] poles = {pole, pole1,pole2};
		
		return poles;
	}
	
	
	//The initial print
	public static void initialPrint(){
		Stack [] stacks = createPoles(gridSize);
		String [][] displayGrid = new String[gridSize][((int) Math.pow(gridSize, 2) + (2))];
			
		//Row
		for(int i = 0; i < displayGrid.length; i++){
			//System.out.println();
			
			//Column
			for(int j = 0; j < displayGrid[i].length; j++){
			displayGrid[i][j] = " ";
			}
		}
				//Pointer for pole 1
				stacks[0].ptr = stacks[0].front;
				
				
				while(stacks[0].ptr != null){
				
				//b will be used for the row, b is found by subtracting grid size (example, 3, which is 3 rows/disks) by the current stack disk position
				//(example, 1, which is the 2nd row in array form) subtracted by 1 to make the grid size in array form
				//3 - 1 - 1 = 1 (Index 1, which is row 2 in normal terms), then print for as long as the disk number size
				int b = gridSize - stacks[0].ptr.diskPosition - 1;
					
					//
					for(int i = 0; i <= stacks[0].ptr.diskNum; i++){
					displayGrid[b][i] = "#";
					}
					stacks[0].ptr = stacks[0].ptr.next;
					
				}
				
				//This will print all the values in displayGrid array
				//Row
				for(int i = 0; i < displayGrid.length; i++){
					System.out.println();
					
					//Column
					for(int j = 0; j < displayGrid[i].length; j++){
						System.out.print(displayGrid[i][j]);
					}
				}
				System.out.println();
				
				//Prints the bottom dashes for making the poles easier to differentiate
				for(int j = 0; j < 3; j++){
				if(j > 0){
				System.out.print(" ");
				}
				for(int i = 0; i < gridSize; i++){
					System.out.print("-");
				}
				}
				
				System.out.println();

				
		
		
	}
	//Handles user input
	public static int userInput(){
		Scanner s = new Scanner(System.in);
		String diskNum = s.nextLine();
		
		//Is the number an integer
		if(diskNum.matches("[0-9]+")){
		    //Checks for valid range
		    if(Integer.parseInt(diskNum) < 3 || Integer.parseInt(diskNum) > 10) {
		    	System.out.println("Invalid input, your input is out of range. Select a number between 3 and 10 (Inclusive): ");
		    	diskNum = Integer.toString(userInput());
		    }
		    //Classic Tower of Hanoi
		    else{
		    	if(Integer.parseInt(diskNum) == 3){
		    		System.out.println("You selected the classic Tower Of Hanoi!");
		    	}
		    	
				numOfSteps(Integer.parseInt(diskNum));
		    	return Integer.parseInt(diskNum);
		    }
		    }
		
		//Checks if input is not a number
	    else if(!diskNum.matches("[0-9]+")){
	    	System.out.println("Your input must be of the type integer (a number). Select a number between 3 and 10 (Inclusive): ");
	    	diskNum = Integer.toString(userInput());
	    }
	    
		return Integer.parseInt(diskNum);
		}
	
	//Stack display
	public static void stackDisplay(int gridSize){
		
		//Get the poles from the solution
		Stack [] stacks = getPoles();
		
		//The display grid array used to print the grid
		String [][] displayGrid = new String[gridSize][((int) Math.pow(gridSize, 2) + (2))];
			
		//Assigns a blank space value to every index in the array
		for(int i = 0; i < displayGrid.length; i++){
			//System.out.println();
			
			//Column
			for(int j = 0; j < displayGrid[i].length; j++){
			displayGrid[i][j] = " ";
			}
		}
		
		//Goes through every stack/pole
		for(int a = 0; a < stacks.length; a++){
			
			//Stack is not empty
			if(stacks[a].front != null){
				
			//Stack/pole 1	
			if(a == 0){
				
				stacks[a].ptr = stacks[a].front;

				while(stacks[a].ptr != null){

				int b = gridSize - stacks[a].ptr.diskPosition - 1;
				
				//Check the comments under initialPrint() to understand how printing works
					for(int i = 0; i <= stacks[a].ptr.diskNum; i++){
				
					displayGrid[b][i] = "#";
					}
					stacks[a].ptr = stacks[a].ptr.next;
					
				}
				}
			
			//Stack/pole 2
			else if(a == 1){
				
				stacks[a].ptr = stacks[a].front;

				while(stacks[a].ptr != null){

				int b = gridSize - stacks[a].ptr.diskPosition - 1;
				
					for(int i = 0; i <= stacks[a].ptr.diskNum; i++){
	
					//Only difference is compensation for the column by including gridSize in the column index
					displayGrid[b][i  + gridSize + 1] = "#";
					}
					stacks[a].ptr = stacks[a].ptr.next;
					
				}
				}
			
			//Stack/pole 3
			else if(a == 2){
				
				stacks[a].ptr = stacks[a].front;

				while(stacks[a].ptr != null){

				int b = gridSize  - 1- stacks[a].ptr.diskPosition;

					for(int i = 0; i <= stacks[a].ptr.diskNum; i++){
					int c = i + (gridSize * 2) + 2;					
					displayGrid[b][c] = "#";
					}
					stacks[a].ptr = stacks[a].ptr.next;
					
				}
				}
			}
			
			//Stack was empty
			else{
				continue;
			}
			}
			
		//System.out.println("Stack Display Reached");
		//Row
		for(int i = 0; i < displayGrid.length; i++){
			System.out.println();
			
			//Column
			for(int j = 0; j < displayGrid[i].length; j++){
				System.out.print(displayGrid[i][j]);
			}
		}
		
		//Dashes print
		System.out.println();
		for(int j = 0; j < 3; j++){
		if(j > 0){
		System.out.print(" ");
		}
		for(int i = 0; i < gridSize; i++){
			System.out.print("-");
		}
		}
		
		System.out.println();

	}
	
	//Number of steps
	public static int numOfSteps(int numDisks){
		steps = (int) (Math.pow(2, numDisks) - 1);
		System.out.println("This will take " + steps + " steps.");
		return steps;
	}
	
	//Solution, solved recursively
	public static void solution(int diskNum, Stack pole, Stack pole1, Stack pole2) throws InterruptedException{
		//System.out.println("Solution Reached");
		
		if(diskNum == 1){
			//Thread sleep to delay the steps from coming out all at once. This will take 3+ seconds (3000 = 3 seconds) to display each step.
			//To print everything at once, comment it out, or to slow it down, lower the value.
			Thread.sleep(3000);

			stepCounter++;
			int top = pole.front.diskNum;
			pole.Pop();
			
			pole2.Push(top);
			
			System.out.print(stepCounter + ": " + pole.name + " --> " + pole2.name);
			
			//Pole assignment for display poses, this recursive method switches them up so they are sent back in the correct order
			for(int i = 0; i < poles.length; i++) {
				if(poles[i].name == "A") {
					displayPoles[0] = poles[i];
				}
				
				else if(poles[i].name == "B") {
					displayPoles[1] = poles[i];
				}
				
				else {
					displayPoles[2] = poles[i];
				}
			}
			
			
			stackDisplay(gridSize);
			
			//Puzzle complete
			if(steps == stepCounter){
				System.out.println("That's all the steps! Thanks for playing Tower Of Christian! :)");
			}
		}
		
		else{
			//Recursive steps
			solution(diskNum - 1, pole, pole2, pole1);
			solution(1, pole, pole1, pole2);
			solution(diskNum - 1, pole1, pole, pole2);
		}
		
	}
}
//Thanks for reading my code! :) ~ CR