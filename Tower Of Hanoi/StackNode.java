
public class StackNode {

	  StackNode next; 
	  StackNode prev;
	  int diskNum;
	  int diskPosition;
	  
	  public StackNode(int diskNum) {
		  this.next = null;
		  this.prev = null;
		  this.diskNum = diskNum;
		  this.diskPosition = 0;
	  }
}
