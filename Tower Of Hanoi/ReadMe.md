This is Tower Of Hanoi! (Also known as Tower Of Christian)

Main Class: TowerOfHanoi.java

Additional Required Classes: Stack.java, StackNode.java

---Data Structure: Stack---

Goal: Here is a description of the original, https://en.wikipedia.org/wiki/Tower_of_Hanoi . This project will complete the original and can handle much more disks (but we've limited it to 10 disks).

Input: The user can select from 3 to 10 disks.

Implementation: Each individual pole is created as a stack. Disks are pushed and popped off the poles in a recursive manner. This way, we will find the solution in the shortest number of steps.

![Tower Of Hanoi Gif](Tower Of Hanoi/TowerOfHanoi.gif)
