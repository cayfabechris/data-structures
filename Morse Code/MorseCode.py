import winsound


print("Welcome to Christian's Morse Code Generator! This is a top secret application!")
print("Enter a string. It will be converted to morse code.")

#List used to store MorseObject items
morseArray = []

#List that will store each character from the user input string
strMorse = []

#User input string
str = ""

#Print each character of the string
def printStringChars():
        for x in strMorse:
            print(x)

#Generate each MorseObject item (letter/number/symbol with morse code and store them in morseArray)
def generateMorseObjects():
    # Alphabet
    m1 = MorseObject("A", "O-")
    morseArray.append(m1)

    m2 = MorseObject("B", "-OOO")
    morseArray.append(m2)

    m3 = MorseObject("C", "-O-O")
    morseArray.append(m3)

    m4 = MorseObject("D", "-OO")
    morseArray.append(m4)

    m5 = MorseObject("E", "O")
    morseArray.append(m5)

    m6 = MorseObject("F", "OO-O")
    morseArray.append(m6)

    m7 = MorseObject("G", "--O")
    morseArray.append(m7)

    m8 = MorseObject("H", "OOOO")
    morseArray.append(m8)

    m9 = MorseObject("I", "OO")
    morseArray.append(m9)

    m10 = MorseObject("J", "O---")
    morseArray.append(m10)

    m11 = MorseObject("K", "-O-")
    morseArray.append(m11)

    m12 = MorseObject("L", "O-OO")
    morseArray.append(m12)

    m13 = MorseObject("M", "--")
    morseArray.append(m13)

    m14 = MorseObject("N", "-O")
    morseArray.append(m14)

    m15 = MorseObject("O", "---")
    morseArray.append(m15)

    m16 = MorseObject("P", "O--O")
    morseArray.append(m16)

    m17 = MorseObject("Q", "--O-")
    morseArray.append(m17)

    m18 = MorseObject("R", "O-O")
    morseArray.append(m18)

    m19 = MorseObject("S", "OOO")
    morseArray.append(m19)

    m20 = MorseObject("T", "-")
    morseArray.append(m20)

    m21 = MorseObject("U", "OO-")
    morseArray.append(m21)

    m22 = MorseObject("V", "OOO-")
    morseArray.append(m22)

    m23 = MorseObject("W", "O--")
    morseArray.append(m23)

    m24 = MorseObject("X", "-OO-")
    morseArray.append(m24)

    m25 = MorseObject("Y", "-O--")
    morseArray.append(m25)

    m26 = MorseObject("Z", "--OO")
    morseArray.append(m26)

    # Numbers
    m27 = MorseObject("1", "O----")
    morseArray.append(m27)

    m28 = MorseObject("2", "OO---")
    morseArray.append(m28)

    m29 = MorseObject("3", "OOO--")
    morseArray.append(m29)

    m30 = MorseObject("4", "OOOO-")
    morseArray.append(m30)

    m31 = MorseObject("5", "OOOOO")
    morseArray.append(m31)

    m32 = MorseObject("6", "-OOOO")
    morseArray.append(m32)

    m33 = MorseObject("7", "--OOO")
    morseArray.append(m33)

    m34 = MorseObject("8", "---OO")
    morseArray.append(m34)

    m35 = MorseObject("9", "----O")
    morseArray.append(m35)

    m36 = MorseObject("0", "-----")
    morseArray.append(m36)

    # Symbols

    m37 = MorseObject(".", "O-O-O-")
    morseArray.append(m37)

    m38 = MorseObject(",", "--OO--")
    morseArray.append(m38)

    m39 = MorseObject("?", "OO--OO")
    morseArray.append(m39)

    m40 = MorseObject("'", "O----O")
    morseArray.append(m40)

    m41 = MorseObject("!", "-O-O--")
    morseArray.append(m41)

    m42 = MorseObject("/", "-OO-O")
    morseArray.append(m42)

    m43 = MorseObject(":", "---OO")
    morseArray.append(m43)

    m44 = MorseObject(";", "-O-O-O")
    morseArray.append(m44)

    m45 = MorseObject("=", "-OOO-")
    morseArray.append(m45)

    m46 = MorseObject("+", "O-O-O")
    morseArray.append(m46)

    m47 = MorseObject("-", "-OOOO-")
    morseArray.append(m47)

    m48 = MorseObject("_", "OO--O-")
    morseArray.append(m48)

    m49 = MorseObject("\"", "O-OO-O")
    morseArray.append(m49)

    m50 = MorseObject("@", "O--O-O")
    morseArray.append(m50)

    m51 = MorseObject(" ", "|")
    morseArray.append(m51)


#Print each MorseObject in morseArray
def printMorseArray():
    for morseObject in morseArray:
        print(morseObject.letter + " " + morseObject.output)

#MorseObject Class
class MorseObject:
    def __init__(self, letter, output):
        self.letter = letter
        self.output = output

    def printMorseObject(self):
        print("Letter: " + self.letter + "\t Output: " + self.output)

    def printCode(self):
        print(self.letter + ":\t" + self.output)


#Decipher the string and play sounds based on MorseObject output
def morseDecipher():
    #print("Morse Decipher reached")
    #print(len(strMorse))

    #For each character in the string
    for i in strMorse:
        #print(i)
        #print(len(morseArray))
        #Compare to each MorseObject in morseArray
        for MorseObject in morseArray:
            #String character match found in morseArray
            if i.capitalize() == MorseObject.letter:
                MorseObject.printCode()

                #print(morseArray.index(MorseObject))
                #print("Match reached")
                #Output sound
                for c in MorseObject.output:
                    if c == 'O':
                        winsound.Beep(440, 1000)
                    if c == '-':
                        winsound.Beep(600, 2000)
                    if c == '|':
                        winsound.Beep(37, 1000)

            else:
                continue


#Execute program
def programExecute():
    generateMorseObjects()
    str = input()
    strMorse.extend(str)
    #printStringChars()
    #print(strMorse)


    #printMorseArray()
    morseDecipher()

    if len(str) > 0:
        print("Here is the string converted to lowercase: " + str.lower())

    print("Thanks for using my program! ~ CR :)")

programExecute()

#Thanks for reading my code! ~ CR :)