This is Morse Code!

---Data Structure: Array---


Goal: Convert a string of text into morse code.

Input: User will enter a string in which each letter/character/number/symbol will be processed through morse code.

Implementation: Each character in the string will be stored into an array. Each character has it's own unique morse code (Example: L in morse code is O-OO). 
The program will run through each character, every character in it's morse code equivalent, and that code will generate a beep based on that character's current morse character.
The sound will be generated using a Python library called winsound.


![Morse Code Video](Morse Code/MorseCode.mp4)